<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

require_once dirname(__FILE__) . '/../../../../../../shell/abstract.php';

class Cron_Test extends Mage_Shell_Abstract
{
	public function run()
	{
		$fakeSchedule = new Varien_Object();

		Mage::getModel('GroupSwitcher/observer')->scheduledGroupSwitches($fakeSchedule);
	}
}

ini_set('display_errors', 1);

$tester = new Cron_Test();

Mage::setIsDeveloperMode(true);

$tester->run();