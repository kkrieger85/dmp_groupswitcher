<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Helper_Data extends Mage_Core_Helper_Abstract
{
	const COMPARISON_EQUAL = 'eq';
	const COMPARISON_GREATER = 'gt';
	const COMPARISON_LESS = 'lt';
	const COMPARISON_GREATER_OR_EQUAL = 'ge';
	const COMPARISON_LESS_OR_EQUAL = 'le';

	const XML_RULE_TYPES_PATH = 'global/groupswitcher/rule/types';

	protected $_ruleTypes;

	/**
	 *
	 * @param string $key
	 * @param int|string|Mage_Core_Model_Store $store
	 * @return string
	 */
	public function getConfig($key, $store = null)
	{
		$path = $this->getConfigBasePath() . '/' . $key;
		return Mage::getStoreConfig($path, $store);
	}

	public function getConfigBasePath()
	{
		return 'dermodpro_groupswitcher/general';
	}

	/**
	 *
	 * @param mixed $message
	 * @param int $level
	 * @return DerModPro_GroupSwitcher_Helper_Data
	 */
    public function log($message, $level = null)
    {
		if ($this->getConfig('enable_logging'))
		{
			Mage::log($message, $level, $this->getConfig('logfile'), true);
		}
        return $this;
    }

	/**
	 *
	 * @param bool $keysOnly
	 * @return array
	 */
	public function getAllRuleTypes($keysOnly = false)
	{
		if (is_null($this->_ruleTypes))
		{
			$this->_ruleTypes = array();
			try
			{
				$node = Mage::getConfig()->getNode(self::XML_RULE_TYPES_PATH);
				if (! $node)
				{
					Mage::throwException(sprintf('Unable to load config node "%s"', self::XML_RULE_TYPES_PATH));
				}
				$types = (array) $node->children();
				foreach ($types as $key => $type)
				{
					$this->_translateConfigNode($type);
					try
					{
						$model = Mage::getModel((string) $type->model);
						if ($model->isAvailable())
						{
							$this->_ruleTypes[$key] = $type->asArray();
						}
					}
					catch (Exception $e)
					{
						if (isset($type->model))
						{
							$this->log(sprintf('Unable to create instance of groupswitcher rule model "%s"', $type->model));
						}
					}
				}
			}
			catch (Exception $e)
			{
				$this->log($e->getMessage());
			}
		}

		if ($keysOnly)
		{
			return array_keys($this->_ruleTypes);
		}
		return $this->_ruleTypes;
	}

	/**
	 *
	 * @return array
	 */
	public function getComparisonOptions($withNullOption = false)
	{
		$options = array(
			array('value' => self::COMPARISON_EQUAL, 'label' => Mage::helper('GroupSwitcher')->__('Equal')),
			array('value' => self::COMPARISON_GREATER, 'label' => Mage::helper('GroupSwitcher')->__('Greater than')),
			array('value' => self::COMPARISON_LESS, 'label' => Mage::helper('GroupSwitcher')->__('Less than')),
			array('value' => self::COMPARISON_GREATER_OR_EQUAL, 'label' => Mage::helper('GroupSwitcher')->__('Greater than or equal')),
			array('value' => self::COMPARISON_LESS_OR_EQUAL, 'label' => Mage::helper('GroupSwitcher')->__('Less than or equal')),
		);
		if ($withNullOption)
		{
			$label = is_string($withNullOption) ? $withNullOption : '-- Please Choose --';
			array_unshift($options, array('value' => '', 'label' => Mage::helper('GroupSwitcher')->__($label)));
		}

		return $options;
	}

	/**
	 *
	 * @return array
	 */
	public function getComparisonOptionHash($withNullOption = false)
	{
		$options = array();
		foreach ($this->getComparisonOptions($withNullOption) as $option)
		{
			$options[$option['value']] = $option['label'];
		}
		return $options;
	}

	/**
	 *
	 * @param Mage_Core_Model_Config_Element $node
	 * @return DerModPro_GroupSwitcher_Helper_Data
	 */
	protected function _translateConfigNode($node)
	{;
		if (isset($node['translate']))
		{
			$items = explode(' ', (string)$node['translate']);
			foreach ($items as $arg)
			{
				if (isset($node->{$arg}))
				{
					$module = isset($node['module']) ? (string)$node['module'] : 'core';
					if (isset($node['module']))
					{
						$node->{$arg} = Mage::helper((string)$node['module'])->__((string) $node->{$arg});
					}
					else
					{
						$node->{$arg} = Mage::helper('core')->__((string) $node->{$arg});
					}
				}
			}
		}
		return $this;
	}

	/**
	 * Check a vat ID number for syntactic correctness
	 *
	 * @param string $vatId
	 * @return bool
	 */
	public function isVatValid($vatId)
	{
		return Mage::getModel('GroupSwitcher/validator_vatId')->isValidVatId($vatId);
	}
    
    /**
     * returns the support-email from config.xml
     * 
     * @return string
     */
    public function getSupportMail()
    {
        return Mage::getStoreConfig('dermodpro_groupswitcher/support/support_mail');
    }
    
    /**
     * Check if admin is using german locale
     * 
     * @return boolean
     */
    public function isAdminUsingGermanLocale()
    {
        return ("de_DE" == Mage::app()->getLocale()->getLocaleCode());
    }
    
    /**
     * get link to documentation
     * 
     * @return string
     */
    public function getDocumentationLink()
    {
        if (true === $this->isAdminUsingGermanLocale()) {
            return Mage::getStoreConfig('dermodpro_groupswitcher/support/documentation/de_DE');
        } else {
            return Mage::getStoreConfig('dermodpro_groupswitcher/support/documentation/en_US');
        }
    }
    
    /**
     * get link to FAQ
     * 
     * @return string
     */
    public function getFaqLink()
    {
        if (true === $this->isAdminUsingGermanLocale()) {
            return Mage::getStoreConfig('dermodpro_groupswitcher/support/faq/de_DE');
        } else {
            return Mage::getStoreConfig('dermodpro_groupswitcher/support/faq/en_US');
        }
    }
}

