<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Adminhtml_Groupswitcher_RuleController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$helper = Mage::helper('GroupSwitcher');
		
		$this->_title($helper->__('Customers'))->_title($helper->__('Group Switcher'))->_title($helper->__('Rules'));

		$this->loadLayout();

		$this->_setActiveMenu('customer/group/groupswitcher_rule');

		$this->_addBreadcrumb($helper->__('Customers'), $helper->__('Customers'));
		$this->_addBreadcrumb($helper->__('Group Switcher'), $helper->__('Group Switcher'));
		$this->_addBreadcrumb($helper->__('Rules'), $helper->__('Rules'));

		$this->renderLayout();
	}

	public function newAction()
	{
		$helper = Mage::helper('GroupSwitcher');
		$this->_title($helper->__('Customers'))->_title($helper->__('Group Switcher'))->_title($helper->__('New Rule'));
		
		$this->loadLayout();

		$this->_addBreadcrumb($helper->__('Customers'), $helper->__('Customers'));
		$this->_addBreadcrumb($helper->__('Group Switcher'), $helper->__('Group Switcher'));
		$this->_addBreadcrumb($helper->__('New Rule'), $helper->__('New Rule'));
		
		$this->renderLayout();
	}

	public function editAction()
	{
		$helper = Mage::helper('GroupSwitcher');
		$model = Mage::getModel('GroupSwitcher/rule');
		$id = $this->getRequest()->getParam('id');
		$errorRedirectPath = '*/*';

		try
		{
			/*
			 * Either id or type parameter has to be set
			 */
			if ($id)
			{
				if (! $model->load($id)->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcher')->__('No record with ID "%s" found.', $id));
				}
			}
			else
			{
				$type = $this->getRequest()->getParam('rule_type');
				if (! $type)
				{
					$errorRedirectPath = '*/*/new';
					Mage::throwException(Mage::helper('GroupSwitcher')->__('No rule type specified'));
				}

				$model->setRuleType($type);
				
				/*
				 * Trigger exception if missing or invalid rule type is set
				 */
				$typeModel = $model->getTypeModel();
			}

			Mage::register('groupswitcher_rule', $model);

			/*
			 * Build the page title
			 */
			if ($model->getId())
			{
				$pageTitle = $helper->__('Edit Rule #%d', $model->getId());
			}
			else
			{
				$pageTitle = $helper->__('New Rule');
			}
			$this->_title($helper->__('Customers'))->_title($helper->__('Group Switcher'))->_title($pageTitle);

			/*
			 * Add various options for the rule type models to add interface updates
			 *
			 * - REMOVED: layout update handle (not needed, leave events, might be useful for someone)
			 * - events
			 */
			//$this->getLayout()->getUpdate()->addHandle('GROUPSWITCHER_RULE_TYPE_' . $rule->getRuleType());

			Mage::dispatchEvent('groupswitcher_load_layout_before', array('rule' => $model));
			$this->loadLayout();

			$this->_addBreadcrumb($helper->__('Customers'), $helper->__('Customers'));
			$this->_addBreadcrumb($helper->__('Group Switcher'), $helper->__('Group Switcher'));
			$this->_addBreadcrumb($pageTitle, $pageTitle);
			
			Mage::dispatchEvent('groupswitcher_render_layout_before', array('rule' => $model));
			$this->renderLayout();
		}
		catch (Mage_Core_Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect($errorRedirectPath);
		}
		catch (Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect($errorRedirectPath);
		}
	}

	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost())
		{
			$data['id'] = $this->getRequest()->getParam('id');
			if ($ruleType = $this->getRequest()->getParam('rule_type'))
			{
				$data['rule_type'] = $ruleType;
			}
			$this->_getSession()->setFormData($data);

			$model = Mage::getModel('GroupSwitcher/rule');

			$id = $this->getRequest()->getParam('id');
			
			try
			{
				if ($id) $model->load($id);
				$model->addData($data);

				/*
				 * Give the rule type a chance to validate and set default
				 * values on the rule model.
				 */
				$model->getTypeModel()->processRuleBeforeSave($model);

				$model->save();

				if (! $model->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcher')->__('Error saving rule'));
				}

				$this->_getSession()->addSuccess(Mage::helper('GroupSwitcher')->__('Rule was successfully saved'));
				$this->_getSession()->setFormData(false);

				if ($this->getRequest()->getParam('back'))
				{
					$params = array('id' => $model->getId(), 'rule_type' => $model->getRuleType());
					if ($activeTab = $this->getRequest()->getParam('active_tab'))
					{
						$params['active_tab'] = $activeTab;
					}
					$this->_redirect('*/*/edit', $params);
				}
				else
				{
					$this->_redirect('*/*/');
				}
			}
			catch (Exception $e)
			{
				$this->_getSession()->addError($e->getMessage());
				$params = array();
				if ($model && $model->getId())
				{
					$params['id'] = $model->getId();
				}
				if ($model && $model->getRuleType())
				{
					$params['rule_type'] = $model->getRuleType();
					$this->_redirect('*/*/edit', $params);
				}
				else
				{
					/*
					 * No rule type set, redirect to new page
					 */
					$this->_redirect('*/*/new');
				}
			}

			return;
		}

		$this->_getSession()->addError(Mage::helper('GroupSwitcher')->__('No data found to save'));
		$this->_redirect('*/*/');
	}

	public function deleteAction()
	{
		$model = Mage::getModel('GroupSwitcher/rule');
		$id = $this->getRequest()->getParam('id');

		try
		{
			if ($id)
			{
				if (! $model->load($id)->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcher')->__('No record with ID "%s" found.', $id));
				}
				$name = $model->getName();
				$model->delete();
				$this->_getSession()->addSuccess(Mage::helper('GroupSwitcher')->__('Rule "%s" (ID %d) was successfully deleted', $name, $id));
				$this->_redirect('*/*');
			}
		}
		catch (Mage_Core_Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect('*/*');
		}
		catch (Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect('*/*');
		}
	}
}

