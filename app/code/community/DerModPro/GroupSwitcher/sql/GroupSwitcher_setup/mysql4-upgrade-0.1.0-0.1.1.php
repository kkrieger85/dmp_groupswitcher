<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

foreach (array('groupswitcher_rule', 'groupswitcher_schedule_switch') as $entityTypeCode)
{
	$entityType = Mage::getModel('eav/entity_type');
	/* @var $entityType Mage_Eav_Model_Entity_Type */

	foreach ($entityType->loadByCode($entityTypeCode)->getAttributeCollection() as $attribute)
	{
		$this->updateAttribute($entityType->getId(), $attribute->getId(), 'frontend_model', 'GroupSwitcher/entity_attribute_frontend_translate_label');
	}
}

$this->endSetup();
