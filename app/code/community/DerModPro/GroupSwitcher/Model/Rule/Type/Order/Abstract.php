<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

abstract class DerModPro_GroupSwitcher_Model_Rule_Type_Order_Abstract
	extends DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
{
	/**
	 *
	 * @return Mage_Sales_Model_Order
	 */
	protected function _getOrder()
	{
		return $this->getObject();
	}

	/**
	 *
	 * @return Mage_Customer_Model_Customer
	 */
	protected function _getCustomer()
	{
		return $this->_getCustomerFromOrder($this->_getOrder());
	}

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Customer_Model_Customer
     */
    protected function _getCustomerFromOrder(Mage_Sales_Model_Order $order)
    {
        if ($order->getCustomer()) return $order->getCustomer();

        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        $order->setCustomer($customer);

        return $customer;
    }

    /**
     *
     * @return bool
     */
    protected function _isValidObject()
    {
        if (! $this->_isValidOrder($this->_getOrder())) return false;

        if (! $this->_isValidCustomer($this->_getCustomer())) return false;

        return parent::_isValidObject();
    }

    /**
     *
     * @return bool
     */
    public function matchRule()
    {
        $order = $this->_getOrder();
        
        if (! $this->_checkOrderState($order)) return false;
        
        return parent::matchRule();
    }
}

