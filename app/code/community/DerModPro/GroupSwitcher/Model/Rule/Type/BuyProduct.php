<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Rule_Type_BuyProduct extends DerModPro_GroupSwitcher_Model_Rule_Type_Order_Abstract
{
    /**
     * Check the rule product has been purchased
     *
     * @return bool
     */
	public function matchRule()
	{
		if (parent::matchRule())
		{
			$order = $this->_getOrder();

			foreach ($this->_getRuleSkus() as $ruleSku)
			{
				foreach ($order->getAllItems() as $item)
				{
					if ($item->getSku() == $ruleSku)
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}

	/**
	 * The rule SKU's must be seperated with a comma and a space.
	 * Other spaces are trimmed.
	 *
	 * @return array
	 */
	protected function _getRuleSkus()
	{
		$ruleSkus = preg_split('/ *, +/', $this->getRule()->getRuleValue1());
		return $ruleSkus;
	}

	/**
	 *
	 * @param array $skus
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_BuyProduct
	 */
	protected function _setRuleSkus(array $skus)
	{
		$this->getRule()->setRuleValue1(implode(", ", $skus));
		return $this;
	}

	/**
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_TurnoverTotal
	 */
	public function processRuleBeforeSave(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		/*
		 * Clean list of skus
		 */
		$ruleSkus = $this->_getRuleSkus();
		sort($ruleSkus);
		$ruleSkus = array_unique($ruleSkus);
		$this->_setRuleSkus($ruleSkus);

		/*
		 * Display notice for not existing SKU's
		 */
		$product = Mage::getModel('catalog/product');
		/* @var $product Mage_Catalog_Model_Product */

		$invalidSkus = array();
		foreach ($ruleSkus as $sku)
		{
			$productId = $product->getIdBySku($sku);
			if (! $productId)
			{
				$invalidSkus[] = $sku;
			}
		}

		if ($invalidSkus)
		{
			$session = Mage::getSingleton('adminhtml/session');
			$session->addNotice(
				Mage::helper('GroupSwitcher')->__('The rule has been saved, but the following SKU\'s currently do not exist:')
			);
			$session->addNotice(implode(', ', $invalidSkus));
		}

		return parent::processRuleBeforeSave($rule);
	}
}

