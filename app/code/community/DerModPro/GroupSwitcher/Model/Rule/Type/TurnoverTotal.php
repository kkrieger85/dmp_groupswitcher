<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Rule_Type_TurnoverTotal extends DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
{
	/**
	 * Match all total customer turnover
	 *
	 * @return bool
	 */
	public function matchRule()
	{
		if (parent::matchRule())
		{
			$comparison = $this->getRule()->getRuleValue1();
			$ruleTotal = $this->getRule()->getRuleValue2();

			$total = $this->_getCustomerTurnover();
			//Mage::helper('GroupSwitcher')->log(sprintf(__METHOD__ . " Comparing: %s %s %s", $total, $comparison, $ruleTotal));

			switch ($comparison)
			{
				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_GREATER_OR_EQUAL:
					return $total >= $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_GREATER:
					return $total > $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_LESS_OR_EQUAL:
					return $total <= $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_LESS:
					return $total < $ruleTotal;

				case DerModPro_GroupSwitcher_Helper_Data::COMPARISON_EQUAL:
					return $total == $ruleTotal;
			}
		}

		return false;
	}

	/**
	 *
	 * @return float
	 */
	protected function _getCustomerTurnover()
	{
		$order = $this->_getOrder();
		$customer = $this->_getCustomer();

		$resource = $order->getResource();
		$adapter = $resource->getReadConnection();
		$select = $adapter->select()
			->from(array('main_table' => $resource->getTable('sales/order')), 'SUM(base_subtotal)')
			->where('main_table.customer_id=?', $customer->getId())
			;
		if ($timeframe = $this->getRule()->getRuleValue3())
		{
			$select->where(sprintf('DATE_SUB(CURDATE(), INTERVAL %d DAY) <= DATE(main_table.created_at)', $timeframe));
		}
		if ($state = $this->getRule()->getOrderState())
		{
			$statuses = $order->getConfig()->getStateStatuses($state, false);
			$select->where('status IN(?)', $statuses);
		}
		$total = $adapter->fetchOne($select);
		//Mage::helper('GroupSwitcher')->log(sprintf('Turnover from orders in state "%s" within the last "%s" days: %s', $state, $timeframe, $total));
		return $total;
	}

	/**
	 * Because this rule type is triggered with an order update or with login
	 * events we need to get the order in two different ways, depending on the
	 * trigger event.
	 *
	 * @return Mage_Sales_Model_Order
	 */
	protected function _getOrder()
	{
		if ($this->getObject() instanceof Mage_Sales_Model_Order)
		{
			/*
			 * Triggered by order update
			 */
			return $this->getObject();
		}

		/*
		 * Triggered by login
		 * Return a dummy order model
		 */
		$order = $this->getData('order');
		if (is_null($order))
		{
			/*
			 * Fake order model
			 */
			$order = Mage::getModel('sales/order')
				->setCustomer($this->_getCustomer())
				->setCustomerId($this->_getCustomer()->getId());
			$this->setOrder($order);
		}
		return $order;
	}

	/**
	 * Because this rule type is triggered with an order update or with login
	 * events we need to get the customer in two different ways, depending on
	 * the trigger event.
	 *
	 * @return Mage_Customer_Model_Customer
	 */
	protected function _getCustomer()
	{
		if ($this->getObject() instanceof Mage_Customer_Model_Customer)
		{
			/*
			 * Triggered by login
			 */
			return $this->getObject();
		}

		/**
		 * Triggered by order update
		 */
		$customer = $this->_getOrder()->getCustomer();
		if (! $customer)
		{
			$customer = Mage::getModel('customer/customer')->load($this->_getOrder()->getCustomerId());
			$this->_getOrder()->setCustomer($customer);
		}
		return $customer;
	}

	/**
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_TurnoverTotal
	 */
	public function processRuleBeforeSave(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		/*
		 * Set a integer value for the last N days
		 */
		if (! is_numeric($rule->getRuleValue3()))
		{
			$rule->setRuleValue3(intval($rule->getRuleValue3()));
		}

		return parent::processRuleBeforeSave($rule);
	}
}

