<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Rule_Type_VatIdNumber extends DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
{
	/**
	 * Check if the customer's vat id validity matches the rules configuration
	 *
	 * @return bool
	 */
	public function matchRule()
	{
		if (parent::matchRule())
		{
			$isValid = $this->_isCustomerVatValid();

			switch ($this->getRule()->getRuleValue2())
			{
				case DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_VatIdNumber::MATCH_TYPE_VALID:
					/*
					 * Match rule if VAT ID is VALID
					 */
					return $isValid;

				case DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_VatIdNumber::MATCH_TYPE_INVALID:
					/*
					 * Match rule if VAT ID is INVALID
					 */
					return ! $isValid;

				case DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_VatIdNumber::MATCH_TYPE_INLAND_VAT:
					/*
					 * Match rule if VAT ID is VALID and same country as the store VAT ID
					 */
					return $isValid && $this->_isCustomerVatInland();

				case DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_VatIdNumber::MATCH_TYPE_NOT_INLAND_VAT:
					/*
					 * Match rule if VAT ID is NOT (VALID and same country as the store VAT ID)
					 */
					return ! ($isValid && $this->_isCustomerVatInland());

				case DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_VatIdNumber::MATCH_TYPE_FOREIGN_VAT:
					/*
					 * Match rule if VAT ID is VALID and different country as the store VAT ID
					 */
					return $isValid && $this->_isCustomerVatForeign();

				case DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_VatIdNumber::MATCH_TYPE_NOT_FOREIGN_VAT:
					/*
					 * Match rule if VAT ID is NOT (VALID and different country as the store VAT ID)
					 */
					return ! ($isValid && $this->_isCustomerVatForeign());
			}
		}
		return false;
	}

	/**
	 * Check if the customers vat id is valid
	 *
	 * @return bool
	 */
	protected function _isCustomerVatValid()
	{
		$customerVatId = $this->_getCustomerVatId();

		if (! Mage::helper('GroupSwitcher')->isVatValid($customerVatId))
		{
			return false;
		}

		if (! $this->_handleXmlRpcValidation($customerVatId))
		{
			return false;
		}
		
		return true;
	}

	/**
	 * Return true if the customers vat id country is the same as the store vat id
	 *
	 * @return bool
	 */
	protected function _isCustomerVatInland()
	{
		$customerVatId = $this->_getCustomerVatId();
		
		return strtoupper(substr($customerVatId, 0, 2)) === substr($this->_getStoreVatId(), 0, 2);
	}

	/**
	 * Return true if the customers vat id country is different from the store vat id
	 *
	 * @return bool
	 */
	protected function _isCustomerVatForeign()
	{
		$customerVatId = $this->_getCustomerVatId();

		return strtoupper(substr($customerVatId, 0, 2)) !== substr($this->_getStoreVatId(), 0, 2);
	}

	/**
	 * Return the configured merchant vat id (might be an empty string)
	 *
	 * @return string
	 */
	protected function _getStoreVatId()
	{
		return (string) Mage::helper('GroupSwitcher')->getConfig('store_vat_id');
	}

	/**
	 * Return the customers attribute value that is configured as the VAT ID
	 *
	 * @return string
	 */
	protected function _getCustomerVatId()
	{
		$attribute = $this->getRule()->getRuleValue1();
		return (string) $this->_getCustomer()->getDataUsingMethod($attribute);
	}

	/**
	 * If the vat id can be checked via xml-rpc (i.e. it is non-german var id)
	 * return the result of the online request.
	 * If the online chack can't be made, return true (so the syntax check
	 * result is used.
	 *
	 * @param string $customerVatId
	 * @return bool
	 */
	protected function _handleXmlRpcValidation($customerVatId)
	{
		/*
		 * Check a german vat id number is configured for the store
		 */
		if ($storeVatId = Mage::helper('GroupSwitcher')->getConfig('store_vat_id'))
		{
			if (strtoupper(substr($storeVatId, 0, 2)) === 'DE')
			{
				if (strtoupper(substr($customerVatId, 0, 2)) !== 'DE')
				{
					try
					{
						/*
						 * Online validation only is available for non-german vat ids
						 */
						return Mage::getModel('GroupSwitcher/validator_vatId_rpc')->isValid($storeVatId, $customerVatId);
					}
					catch (Exception $e)
					{
						Mage::helper('GroupSwitcher')->log($e->getMessage());
					}
				}
			}
		}

		/*
		 * No online validation available
		 */
		return true;
	}
}

