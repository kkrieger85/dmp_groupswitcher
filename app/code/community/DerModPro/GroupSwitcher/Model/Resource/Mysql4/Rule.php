<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule extends Mage_Core_Model_Mysql4_Abstract
{
	/**
	 * The entity type code that getAttributes() returns the attributes for
	 *
	 * @var string
	 */
	protected $_entityTypecode = 'groupswitcher_rule';

	/**
	 * Initialize table and id column
	 */
	protected function _construct()
	{
		$this->_init('GroupSwitcher/rule', 'entity_id');
	}

	/**
	 * Return the rule attribute models with the attribute group names set.
	 * Ignore attribute sets - this is a flat table.
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return array
	 */
	public function getAttributes(DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		$entityType = Mage::getModel('eav/entity_type')->loadByCode($this->_entityTypecode);
		$collection = $entityType->getAttributeCollection()->load();

		/*
		 * Add attribute group information
		 */
		$table = $this->getTable('eav/entity_attribute');
		$select = $this->getReadConnection()->select()
			->from($table, 'attribute_id')
			->joinInner(
					array('g' => $this->getTable('eav/attribute_group')),
					"`{$table}`.`attribute_group_id` = g.`attribute_group_id`",
					array('attribute_group_name'))
			->where("`{$table}`.entity_type_id = ?", $entityType->getEntityTypeId())
			->order("{$table}.attribute_group_id ASC")
			->order("{$table}.sort_order ASC")
		;
		$result = $this->getReadConnection()->fetchPairs($select);

		$attributes = array();

		foreach ($result as $id => $group)
		{
			if ($attribute = $collection->getItemById($id))
			{
				$attribute->setGroup($group);
				$attributes[$attribute->getAttributeCode()] = $attribute;
			}
		}
		
		return $attributes;
	}

	/**
	 *
	 * @param Mage_Core_Model_Abstract $rule
	 * @return DerModPro_GroupSwitcher_Model_Resource_Mysql4_Rule
	 */
	protected function _beforeSave(Mage_Core_Model_Abstract $rule)
	{
		if (! $rule->getId())
		{
			$rule->setCreatedAt(now());
		}
		$rule->setUpdatedAt(now());
		return parent::_beforeSave($rule);
	}
}