<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	/**
	 * @var string
	 */
	protected $_customerResourceModel;

	/**
	 * @var string
	 */
	protected $_customerTableName;

	public function _construct()
	{
		$this->_init('GroupSwitcher/schedule_switch');
	}

	/**
	 *
	 * @param string|Zend_Db_Expr $date
	 * @return DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch_Collection
	 */
	public function addDateFilter($date = null)
	{
		if (is_null($date))
		{
			/*
			 * Don't use the locale date here because this process runs for all websites / stores / views
			 */
			$date = new Zend_Db_Expr('CURDATE()');
		}
		elseif (is_string($date) && ($timestamp = strtotime($date)))
		{
			$date = date('Y-m-d', $timestamp);
		}
		$this->addFieldToFilter('date', array('eq' => $date));

		return $this;
	}

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch_Collection
	 */
	public function addCustomerData()
	{
		foreach (array('firstname', 'lastname', 'email') as $attributeCode)
		{
			$this->joinCustomerAttribute($attributeCode);
		}
		$this->getSelect()->from('', array(
			'name' => new Zend_Db_Expr("CONCAT(_firstname_table.value, ' ', _lastname_table.value)")
		));
		return $this;
	}

	/**
	 *
	 * @param string $attributeCode
	 * @return DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch_Collection
	 */
	public function joinCustomerAttribute($attributeCode)
	{
		$customerTable = $this->_joinCustomerTable()->_getCustomerTableName();
		
		$attr = $this->_getCustomerAttribute($attributeCode);
		if ($attr->isStatic())
		{
			$this->getSelect()->from('', $customerTable . '.' . $attributeCode);
		}
		else
		{
			$alias = '_' . $attributeCode . '_table';
			$this->getSelect()->joinLeft(
				array($alias => $attr->getBackendTable()),
				'main_table.`customer_id`='.$alias.'.entity_id AND '.
					$alias.'.attribute_id='.$attr->getId().' AND '.
					$alias.'.entity_type_id='.$attr->getEntityTypeId(),
				array($attributeCode => $alias.'.value')
			);
		}
		return $this;
	}

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_Resource_Mysql4_Schedule_Switch_Collection
	 */
	protected function _joinCustomerTable()
	{
		/*
		 * Only join the customer table once.
		 * We can't use $this->join() here because the table isn't configured within the
		 * GroupSwitcher entity table configuration section :-(
		 */
		$customerTable = $this->_getCustomerTableName();
        if (!isset($this->_joinedTables[$customerTable])) {
            $this->getSelect()->join(
				$customerTable,
				'main_table.customer_id='. $customerTable . '.' . $this->_getCustomerResource()->getIdFieldName(),
				''
			);
            $this->_joinedTables[$customerTable] = true;
        }
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	protected function _getCustomerTableName()
	{
		if (is_null($this->_customerTableName))
		{
			$this->_customerTableName = $this->getTable('customer/entity');
		}
		return $this->_customerTableName;
	}

	/**
	 *
	 * @param string $code
	 * @return Mage_Customer_Model_Attribute
	 */
	protected function _getCustomerAttribute($code)
	{
		return Mage::getModel('customer/attribute')->loadByCode('customer', $code);
	}

	/**
	 *
	 * @return Mage_Customer_Model_Entity_Customer
	 */
	protected function _getCustomerResource()
	{
		if (is_null($this->_customerResourceModel))
		{
			$this->_customerResourceModel = Mage::getModel('customer/customer')->getResource();
		}
		return $this->_customerResourceModel;
	}
}