<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Entity_Attribute_Source_Customer_Group_None extends Mage_Customer_Model_Customer_Attribute_Source_Group
{
    public function getAllOptions()
	{
		$options = parent::getAllOptions();
		array_unshift($options, array(
			'value' => '',
			'label' => Mage::helper('GroupSwitcher')->__('-- No Change --'),
		));
		return $options;
	}
}
