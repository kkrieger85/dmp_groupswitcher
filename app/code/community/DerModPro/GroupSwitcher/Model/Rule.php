<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Model_Rule extends Mage_Core_Model_Abstract
{
	protected $_eventPrefix = 'groupswitcher_rule';

	protected $_eventObject = 'rule';

	protected $_typeModel;

	protected function _construct()
	{
		parent::_construct();
		$this->_init('GroupSwitcher/rule');
	}

	/**
	 *
	 * @param Varien_Object $object
	 * @return DerModPro_GroupSwitcher_Model_Rule_Type_Interface
	 */
	public function process(Varien_Object $object = null)
	{
		return $this->getTypeModel()->process($object);
	}

    /**
     *
     * @return DerModPro_GroupSwitcher_Model_Rule_Type_Abstract
     */
	public function getTypeModel()
	{
		if (is_null($this->_typeModel))
		{
			$modelClass = (string) Mage::getConfig()->getNode(DerModPro_GroupSwitcher_Helper_Data::XML_RULE_TYPES_PATH . '/' . $this->getRuleType() . '/model');

			if (! $modelClass)
			{
				Mage::throwException(Mage::helper('GroupSwitcher')->__('Unable to find model for rule type "%s"', $this->getRuleType()));
			}

			$ruleType = Mage::getModel($modelClass)
				->setRule($this);
			
			$this->setTypeModel($ruleType);
		}
		return $this->_typeModel;
	}

	/**
	 *
	 * @param DerModPro_GroupSwitcher_Model_Rule_Type_Interface $ruleType
	 * @return DerModPro_GroupSwitcher_Model_Rule 
	 */
	public function setTypeModel(DerModPro_GroupSwitcher_Model_Rule_Type_Interface $ruleType)
	{
		$this->_typeModel = $ruleType;
		return $this;
	}

    /**
     *
     * @return string
     */
	public function getTypeBlockClass()
	{
		$blockClass = $this->getTypeModel()->getBlockClass();

		return $blockClass;
	}

    /**
     *
     * @return array
     */
	public function getStoreIds()
	{
		$data = $this->getData('store_ids');
		if (is_string($data))
		{
			$data = explode(',', $data);
			$this->setData('store_ids', $data);
		}
		return $data;
	}

	protected function _beforeSave()
	{
		/*
		 * Check values are positive or NULL
		 */
		
		$groupIdBefore = $this->getGroupIdBefore();
		if (isset($groupIdBefore) && ! $groupIdBefore)
		{
			$this->setGroupIdBefore(null);
		}

		$orderState = $this->getOrderState();
		if (isset($orderState) && !$orderState)
		{
			$this->setOrderState(null);
		}

		$storeIds = $this->getStoreIds();
		if (is_array($storeIds))
		{
			$this->setStoreIds(implode(',', $storeIds));
		}

        $groupIdBefore = $this->getScheduleGroupIdBefore();
		if (isset($groupIdBefore) && ! $groupIdBefore)
		{
			$this->setScheduleGroupIdBefore(null);
		}

        $groupIdAfter = $this->getScheduleGroupIdAfter();
		if (isset($groupIdAfter) && ! $groupIdAfter)
		{
			$this->setScheduleGroupIdAfter(null);
		}

        $days = $this->getScheduleSwitchDays();
		if (isset($days) && ! $days)
		{
			$this->setScheduleSwitchDays(null);
		}
		
		return parent::_beforeSave();
	}

	/**
	 *
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->getResource()->getAttributes($this);
	}
}
