<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'id';
		$this->_blockGroup = 'GroupSwitcher';
		$this->_controller = 'adminhtml_rule';
		$this->_mode = 'edit';

		/**
		 * @see DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tabs::__construct() setId() call.
		 */
		$jsObjNamePrefix = 'rule';

		$this->_updateButton('save', 'label', Mage::helper('GroupSwitcher')->__('Save Rule'));
		$this->_updateButton('delete', 'label', Mage::helper('GroupSwitcher')->__('Delete Rule'));

		$this->_addButton('save_and_continue', array(
				'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
				'onclick' => 'saveAndContinueEdit()',
				'class' => 'save',
		), -100);

        $this->_formScripts[] = "
			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/active_tab/' + ruleJsTabs.activeTab.id.replace(/{$jsObjNamePrefix}_/, ''));
			}
		";
    }

	public function getHeaderText()
	{
		if (Mage::registry('groupswitcher_rule') && Mage::registry('groupswitcher_rule')->getId())
		{
			return Mage::helper('GroupSwitcher')->__('Edit Rule #%d "%s"', Mage::registry('groupswitcher_rule')->getId(), $this->htmlEscape(Mage::registry('groupswitcher_rule')->getName()));
		}
		else
		{
			return Mage::helper('GroupSwitcher')->__('New "%s" rule',
				$this->htmlEscape(Mage::registry('groupswitcher_rule')->getTypeModel()->getLabel())
			);
		}
	}
}