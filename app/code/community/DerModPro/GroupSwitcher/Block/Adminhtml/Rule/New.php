<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'id';
		$this->_blockGroup = 'GroupSwitcher';
		$this->_controller = 'adminhtml_rule';
		$this->_mode = 'new';

		$this->_removeButton('save');
		$this->_removeButton('delete');

		/*
		$this->_addButton('continue', array(
				'label' => Mage::helper('GroupSwitcher')->__('Continue'),
				'onclick' => 'editForm.submit()',
				'class' => 'save',
		), -100);
		 */
    }

	public function getHeaderText()
	{
		return Mage::helper('GroupSwitcher')->__('New Rule');
	}
}