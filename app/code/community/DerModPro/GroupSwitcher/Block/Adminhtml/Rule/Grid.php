<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('ruleGrid');
		$this->setUseAjax(false);
		$this->setDefaultSort('entity_id');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel('GroupSwitcher/rule_collection');
		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('entity_id', array(
			'header' => Mage::helper('GroupSwitcher')->__('ID'),
			'width' => '50px',
			'index' => 'entity_id',
			'type' => 'number',
		));
		$this->addColumn('rule_type', array(
			'header' => Mage::helper('GroupSwitcher')->__('Rule Type'),
			'width' => '160px',
			'index' => 'rule_type',
			'renderer' => 'adminhtml/widget_grid_column_renderer_options',
			'filter' => 'adminhtml/widget_grid_column_filter_select',
			'sortable' => false,
			'options' => $this->_getRuleTypeOptions(),
		));
		$this->addColumn('name', array(
			'header' => Mage::helper('GroupSwitcher')->__('Rule Name'),
			'index' => 'name',
		));

		$this->addColumn('is_active', array(
			'header' => Mage::helper('GroupSwitcher')->__('Active'),
			'width' => '50px',
			'index' => 'is_active',
			'type' => 'options',
			'options' => $this->_getYesNoOptions(),
		));

		$this->addColumn('group_before', array(
			'header' => Mage::helper('GroupSwitcher')->__('From Group'),
			'index' => 'group_id_before',
			'type' => 'options',
			'options' => $this->_getGroupBeforeOptions(),
		));

		$this->addColumn('group_after', array(
			'header' => Mage::helper('GroupSwitcher')->__('To Group'),
			'index' => 'group_id_after',
			'type' => 'options',
			'options' => $this->_getGroupAfterOptions(),
		));

		$this->addColumn('send_email', array(
			'header' => Mage::helper('GroupSwitcher')->__('Send Email'),
			'width' => '50px',
			'index' => 'send_email',
			'type' => 'options',
			'options' => $this->_getYesNoOptions(),
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper('GroupSwitcher')->__('Created At'),
			'type' => 'datetime',
			'align' => 'center',
			'index' => 'created_at',
			'gmtoffset' => true,
		));
		
		if (! Mage::app()->isSingleStoreMode())
		{
			$this->addColumn('store_ids', array(
				'header' => Mage::helper('GroupSwitcher')->__('Stores'),
				'align' => 'center',
				'width' => '80px',
				'renderer' => 'GroupSwitcher/adminhtml_widget_grid_column_renderer_stores',
				'type' => 'options',
				'options' => Mage::getSingleton('adminhtml/system_store')->getStoreOptionHash(),
				'index' => 'store_ids',
				'sortable' => false,
				'filter' => false,
			));
		}

		$this->addColumn('action', array(
			'header' => Mage::helper('GroupSwitcher')->__('Action'),
			'width' => '100',
			'type' => 'action',
			'getter' => 'getId',
			'actions' => array(
				array(
					'caption' => Mage::helper('GroupSwitcher')->__('Edit'),
					'url' => array('base' => '*/*/edit'),
					'field' => 'id'
				)
			),
			'filter' => false,
			'sortable' => false,
			'index' => 'stores',
			'is_system' => true,
		));
		
		return parent::_prepareColumns();
	}

	//public function getGridUrl()
	//{
	//	return $this->getUrl('*/*/grid', array('_current' => true));
	//}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

	protected function _getYesNoOptions()
	{
		return array(
			1 => Mage::helper('GroupSwitcher')->__('Yes'),
			0 => Mage::helper('GroupSwitcher')->__('No'),
		);
	}

	protected function _getGroupOptions()
	{
		$groups = $this->getCustomerGroupHash();
		if (is_null($groups))
		{
			$groups = Mage::getResourceModel('customer/group_collection')
				->addFieldToFilter('customer_group_id', array('gt'=> 0))
				->load()
				->toOptionHash();
			$this->setCustomerGroupHash($groups);
		}
		return $groups;
	}

	protected function _getGroupBeforeOptions()
	{
		$groups = $this->_getGroupOptions();
		ksort($groups);
		return $groups;
	}

	protected function _getGroupAfterOptions()
	{
		$groups = $this->_getGroupOptions();
		$groups['0'] = Mage::helper('GroupSwitcher')->__('-- No Change --');
		ksort($groups);
		return $groups;
	}

	protected function _getRuleTypeOptions()
	{
		$options = array();
		$types = Mage::helper('GroupSwitcher')->getAllRuleTypes();
		foreach ($types as $value => $type)
		{
			if (isset($type['hidden']) && $type['hidden'])
			{
				continue;
			}
			$options[$value] = Mage::helper('GroupSwitcher')->__($type['label']);
		}
		return $options;
	}
}

