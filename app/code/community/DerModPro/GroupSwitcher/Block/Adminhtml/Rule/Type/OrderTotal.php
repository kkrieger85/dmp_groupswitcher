<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_OrderTotal
	extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_Abstract
{
	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('type');
		$this->setForm($form);

		$fieldset = $form->addFieldset('type_form', array(
			'legend' => $this->_getRuleModel()->getTypeModel()->getLabel()
		));

		$fieldset->addField('rule_value1', 'select', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Order Subtotal is'),
			'note'    => Mage::helper('GroupSwitcher')->__('Compare the order subtotal using this operator'),
			'name'     => 'rule_value1',
			'options'  => Mage::helper('GroupSwitcher')->getComparisonOptionHash(true),
			'required' => 1,
		));

		$fieldset->addField('rule_value2', 'text', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Amount'),
			'name'     => 'rule_value2',
			'required' => 1,
		));

		$attributes = array($this->_getAttributeByCode('order_state'));

        $this->_setFieldset($attributes, $fieldset);

		$fieldset->addField('store_ids', 'multiselect', array(
			'label' => 'Stores',
			'name' => 'store_ids',
			'values' => $this->_getStoreSelectorOptions(),
		));

        $form->addValues($this->_getFormData());

		return parent::_prepareForm();
	}
}
