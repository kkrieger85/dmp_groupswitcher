<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_NumOrders
	extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_Abstract
{
	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('type');
		$this->setForm($form);

		$fieldset = $form->addFieldset('type_form', array(
			'legend' => $this->_getRuleModel()->getTypeModel()->getLabel()
		));

		$fieldset->addField('rule_value1', 'select', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Number of Orders are'),
			'note'     => Mage::helper('GroupSwitcher')->__('Compare the number of orders of the customer using this operator to the value below'),
			'name'     => 'rule_value1',
			'options'  => Mage::helper('GroupSwitcher')->getComparisonOptionHash(true),
			'required' => 1,
		));

		$fieldset->addField('rule_value2', 'text', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Order Count'),
			'note'     => Mage::helper('GroupSwitcher')->__('Compare the customers number of orders to this value'),
			'name'     => 'rule_value2',
			'required' => 1,
		));

		$fieldset->addField('rule_value3', 'text', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Within the last N Days'),
			'note'     => Mage::helper('GroupSwitcher')->__('Set this value to zero for no time limit'),
			'name'     => 'rule_value3',
			'required' => 0,
		));

		$orderStateAttribute = $this->_getAttributeByCode('order_state')
				->setNote('Only count orders matching this order state');
		$attributes = array($orderStateAttribute);

        $this->_setFieldset($attributes, $fieldset);
        
        $fieldset->addField('store_ids', 'multiselect', array(
            'label' => 'Stores',
            'name' => 'store_ids',
            'values' => $this->_getStoreSelectorOptions(),
        ));
        
        $form->addValues($this->_getFormData());

		return parent::_prepareForm();
	}
}

