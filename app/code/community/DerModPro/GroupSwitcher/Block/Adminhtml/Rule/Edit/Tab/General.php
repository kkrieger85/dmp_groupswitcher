<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_General extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Abstract
{
	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('general');

		
		$fieldset = $form->addFieldset('general_form', array(
			'legend' => Mage::helper('GroupSwitcher')->__('General Setup')
		));

		$attributes = $this->_getGroupAttributes('General');

		if (! $this->_getRuleModel()->getId())
		{
			/*
			 * Set the field type for the  rule_type attribute for new rules to hidden
			 */
			$fieldset->addField('rule_type', 'hidden', array(
				'name' => 'rule_type'
			));
		}
		
        $this->_setFieldset($attributes, $fieldset);
        $form->addValues($this->_getFormData());

		if ($this->_getRuleModel()->getId())
		{
			$form->addValues(array(
				'rule_type' => $this->_getRuleModel()->getTypeModel()->getLabel()
			));
		}

		$this->setForm($form);


		/*
		 * Define field dependencies
		 */
		$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
			->addFieldMap("generalsend_email", 'send_email')
			->addFieldMap("generalemail_template", 'email_template')
			->addFieldDependence('email_template', 'send_email', '1')
		);

		return parent::_prepareForm();
	}
}