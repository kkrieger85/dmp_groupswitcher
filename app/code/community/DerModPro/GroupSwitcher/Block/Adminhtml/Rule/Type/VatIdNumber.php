<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_VatIdNumber
	extends DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Type_Abstract
{
	const MATCH_TYPE_NOT_FOREIGN_VAT = 5;
	const MATCH_TYPE_NOT_INLAND_VAT  = 4;
	const MATCH_TYPE_FOREIGN_VAT     = 3;
	const MATCH_TYPE_INLAND_VAT      = 2;
	const MATCH_TYPE_VALID           = 1;
	const MATCH_TYPE_INVALID         = 0;

	protected $_defaultAttribute = 'taxvat';

	protected $_defaultMatchType = self::MATCH_TYPE_VALID;

	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('type');
		$this->setForm($form);

		$fieldset = $form->addFieldset('type_form', array(
			'legend' => $this->_getRuleModel()->getTypeModel()->getLabel()
		));

		$options = $this->_getCustomerAttributesAsOptions();
		$fieldset->addField('rule_value1', 'select', array(
			'label'    => Mage::helper('GroupSwitcher')->__('VAT ID Number Attribute'),
			'name'     => 'rule_value1',
			'options'  => $options,
			'required' => 1,
		));

		$fieldset->addField('rule_value2', 'select', array(
			'label'    => Mage::helper('GroupSwitcher')->__('Apply rule if'),
			'name'     => 'rule_value2',
			'options'  => $this->_getVatValidOptions(),
			'required' => 1,
			'note'     => Mage::helper('GroupSwitcher')->__('Foreign / same country checks are performed by comparing the customers VAT ID with the value configured in System > Configuration > GroupSwitcher > <a href="%s">Store VAT Identification Number</a>',
					Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit', array('section' => 'dermodpro_groupswitcher')))
		));

		/*
		 * Set default values
		 */
		$data = $this->_getFormData();
		if (! isset($data['rule_value1']) && isset($options[$this->_defaultAttribute]))
		{
			$data['rule_value1'] = $this->_defaultAttribute;
		}
		if (! isset($data['rule_value2']))
		{
			$data['rule_value2'] = $this->_defaultMatchType;
		}

        $form->addValues($data);

		return parent::_prepareForm();
	}

	protected function _getCustomerAttributesAsOptions()
	{
		$customerEntityType = Mage::getModel('eav/entity_type')->loadByCode('customer');
		$options = array(
			'' => Mage::helper('GroupSwitcher')->__('-- Please Choose --')
		);
		foreach ($customerEntityType->getAttributeCollection() as $attribute)
		{
			if ($attribute->getFrontendLabel())
			{
				$options[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
			}
		}
		asort($options);
		return $options;
	}

	protected function _getVatValidOptions()
	{
		$options = array();
		if (Mage::helper('GroupSwitcher')->getConfig('store_vat_id'))
		{
			$options[self::MATCH_TYPE_FOREIGN_VAT] = Mage::helper('GroupSwitcher')->__('VAT ID is valid and from foreign EU country');
			$options[self::MATCH_TYPE_NOT_FOREIGN_VAT] = Mage::helper('GroupSwitcher')->__('VAT ID is not valid and from foreign EU country');
			$options[self::MATCH_TYPE_INLAND_VAT] = Mage::helper('GroupSwitcher')->__('VAT ID is valid and from same EU country');
			$options[self::MATCH_TYPE_NOT_INLAND_VAT] = Mage::helper('GroupSwitcher')->__('VAT ID is not valid and from same EU country');
		}
		$options[self::MATCH_TYPE_VALID]   = Mage::helper('GroupSwitcher')->__('VAT ID is valid');
		$options[self::MATCH_TYPE_INVALID] = Mage::helper('GroupSwitcher')->__('VAT ID is not valid');
		
		return $options;
	}
}
