<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	/**
	 * Default block for rule type attributes tab
	 *
	 * @var string
	 */
	protected $_defaultRuleTypeBlockClass = 'GroupSwitcher/adminhtml_rule_edit_tab_type';
	
	public function __construct()
	{
		parent::__construct();
		$this->setId('rule');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('GroupSwitcher')->__('Rule Setup'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('general_section', array(
				'label'   => Mage::helper('GroupSwitcher')->__('General'),
				'title'   => Mage::helper('GroupSwitcher')->__('General'),
				'content' => $this->getLayout()->createBlock('GroupSwitcher/adminhtml_rule_edit_tab_general')->toHtml(),
		));

		$this->addTab('type_section', array(
				'label'   => Mage::helper('GroupSwitcher')->__('Type Specific'),
				'title'   => Mage::helper('GroupSwitcher')->__('Type Specific'),
				'content' => $this->_getRuleTypeBlock()->toHtml(),
		));
		
		$this->addTab('schedule_section', array(
				'label'   => Mage::helper('GroupSwitcher')->__('Scheduling'),
				'title'   => Mage::helper('GroupSwitcher')->__('Scheduling'),
				'content' => $this->getLayout()->createBlock('GroupSwitcher/adminhtml_rule_edit_tab_schedule')->toHtml(),
		));

		return parent::_beforeToHtml();
	}

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Block_Adminhtml_Rule_Edit_Tab_Abstract
	 */
	protected function _getRuleTypeBlock()
	{
		$class = Mage::registry('groupswitcher_rule')->getTypeBlockClass();
		if ($class)
		{
			if ($blockClass = Mage::getConfig()->getBlockClassName($class))
			{
				if (class_exists($blockClass, false) || mageFindClassFile($blockClass))
				{
					$block = $this->getLayout()->createBlock($class);
				}
			}
		}

		if (! isset($block) || ! $block)
		{
			$block = $this->getLayout()->createBlock($this->_defaultRuleTypeBlockClass);
		}

		return $block;
	}
}