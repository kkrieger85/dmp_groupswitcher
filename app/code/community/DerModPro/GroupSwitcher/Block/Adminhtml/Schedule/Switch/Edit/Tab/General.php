<?php
/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

class DerModPro_GroupSwitcher_Block_Adminhtml_Schedule_Switch_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
	/**
	 * Cache all loaded attribute models
	 *
	 * @var array
	 */
	protected $_attributes;

	protected function _prepareForm()
	{
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('general');
		$data = $this->_getFormData();

		$fieldset = $form->addFieldset('general_form', array(
			'legend' => Mage::helper('GroupSwitcher')->__('General Setup')
		));

        $attributes = $this->_getAttributes();
        // Localize the notification properly
        $customer_data = $attributes['customer_id']->getData();
        $customer_data['note'] = Mage::helper('GroupSwitcher')->__($customer_data['note']);
        $attributes['customer_id']->setData($customer_data);

        $this->_setFieldset($attributes, $fieldset);

		if (! $this->_getSwitchModel()->getId())
		{
			$fieldset->removeField('created_at');
			$fieldset->removeField('updated_at');
		}
		if (! Mage::getSingleton('customer/config_share')->isWebsiteScope())
		{
			$fieldset->removeField('customer_website_id');
		}
		else
		{
			if (isset($data['customer_id']) && is_numeric($data['customer_id']))
			{
				$customer = Mage::getModel('customer/customer')->load($data['customer_id']);
				if ($customer->getId())
				{
					$data['customer_id'] = $customer->getEmail();
				}
			}
		}

        $form->addValues($data);

		$this->setForm($form);


		/*
		 * Define field dependencies
		 */
		$this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
			->addFieldMap("generalsend_email", 'send_email')
			->addFieldMap("generalemail_template", 'email_template')
			->addFieldDependence('email_template', 'send_email', '1')
		);

		return parent::_prepareForm();
	}

	/**
	 *
	 * @return DerModPro_GroupSwitcher_Model_Schedule_Switch
	 */
	protected function _getSwitchModel()
	{
		return Mage::registry('groupswitcher_switch');
	}

	/**
	 *
	 * @return array
	 */
	protected function _getFormData()
	{
		$data = Mage::getSingleton('adminhtml/session')->getFormData();

		if (! $data && $this->_getSwitchModel()->getId())
		{
			$data = $this->_getSwitchModel()->getData();
		}

		return (array) $data;
	}

	/**
	 *
	 * @return array
	 */
	protected function _getAttributes()
	{
		if (is_null($this->_attributes))
		{
			$this->_attributes = $this->_getSwitchModel()->getAttributes();
		}
		return $this->_attributes;
	}

	/**
	 *
	 * @param string $code
	 * @return Mage_Eav_Model_Entity_Attribute
	 */
	protected function _getAttributeByCode($code)
	{
		$attributes = $this->_getAttributes();
		if (isset($attributes[$code]))
		{
			return $attributes[$code];
		}

		return false;
	}
}

