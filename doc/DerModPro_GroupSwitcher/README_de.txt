/**
 * Der Modulprogrammierer - Magento App Factory AG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcher
 * @copyright  Copyright (c) 2012 Der Modulprogrammierer - Magento App Factory AG
 * @license    Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 */

==== ÜBER DIE ERWEITERUNG ====
Diese Erweiterung ermöglicht es Ihnen, Kunden automatisch einer Kundengruppe
zuzuweisen. Die Regeln dazu können flexibel konfiguriert werden.

Die folgenden Regel-Arten stehen zur Verfügung:

- Kauf eines Produktes
Kauft ein Kunde eines der angegebenen Produkte wird er automatisch der konfigurierten
Kundengruppe zugewiesen. Dabei kann ein Bestell-Status angegeben werden den die
Bestellung haben muss (Neu, Verarbeitung, Vollständig, ...), damit die
Gruppenzuweisung ausgeführt wird. Es werden alle Produktarten unterstützt.

- Anzahl Bestellungen
Sobald die Anzahl der Bestellungen eines Kunden die angegebene Menge erreicht
wir er der konfigurierten Gruppe zugewiesen. Die gewerteten Bestellungen können
auf einen Status eingeschränkt werden (Neu, Verarbeitung, Vollständig, ...).
Auch der Zeitraum in denen die Anzahl an Bestellungen erreicht werden soll
kann festgelegt werden (z.B. nur Bestellungen in den letzten 60 Tagen).
Auch wenn die Anzahl der Bestellungen unter einen Wert fällt kann eine
automatische Gruppenzuweisung stattfinden.

- Zwischensumme einer Bestellung
Wenn die Zwischensumme einer Bestellung über (oder unter) einem Wert
liegt, wird der Kunde der konfigurierten Kundengruppe zugewiesen. Damit lassen
sich zum Beispiel Regeln definieren, dass Kunden besondere Rabatte bekommen
solange die letzte Bestellung einen bestimmten Wert überschreitet.
Dabei kann ein Bestell-Status angegeben werden den die
Bestellung haben muss (Neu, Verarbeitung, Vollständig, ...), damit die
Gruppenzuweisung ausgeführt wird.

- Umsatz eines Kunden
Dieser Regel-Typ kann auswerten ob der Gesamtumsatz eines Kunden über (oder
unter) einem Betrag liegt. Dabei wird die Zwischensumme ausgewertet, also ohne
Versandkosten (und je nach Konfiguration des Shops auch ohne Steuern).
Der Zeitraum, innerhalb dessen Bestellungen gewertet werden, kann bestimmt
werden. Auch der Status der gewerteten Bestellungen kann bestimmt werden (z.B.
nur Vollständige Bestellungen).

- Kunden-Attribut
Wenn ein Attribut eines Kunden dem angegebenen Wert entspricht, wird er der
ausgewählten Kundengruppe zugewiesen. Der Wert kann als einfacher Vergleich
angegeben werden, oder als regulärer Ausdruck, oder - falls Sie Magento
Entwickler Know-How zur Hand haben - als Model Callback. Die Syntax dafür wird
weiter unten in dem Abschnitt ANWENDUNG beschrieben.

- Umsatzsteuer-Identifikationsnummer
Dieser Regel-Typ ist vor allem für Shop-Betreiber innerhalb der Europäischen
Gemeinschaft von Nutzen. Gibt ein Kunde eine gültige Umsatzsteuer-Id an, kann
er automatisch einer Kundengruppe mit der entsprechenden Steuerklasse zugewiesen
werden.
Besitzt der Shop-Betreiber eine Deutsche Umsatzsteuer-Id, können die Umsatzsteuer-
Id's von Kunden aus anderen EG-Ländern durch den
XML-RPC Service von http://evatr.bff-online.de/ online validiert werden.
Bitte lesen Sie unter KONFIGURATION und ANWENDUNG wie Sie das Feature aktivieren
können.


Alle Regeln unterstützen die Spezifikation einer Kundengruppe, die der Kunde
angehören muss, damit die Regel angewendet wird.

Ein weiteres Feature ist das ein Gruppenwechsel zu einem Zeitpunkt in der Zukunft
konfiguriert werden kann, wenn die Regelbedingungen zutreffen. Ein möglicher
Anwendungsfall könnte sein, dass ein Kunde durch einen Produktkauf für 30 Tage
einer Gruppe mit einer besseren Rabattstufe zugeordnet wird. Nach 30 Tagen wird
der Kunde dann automatisch wieder der vorherigen Kundengruppe zugeordnet.

Des weiteren unterstützt diese Erweiterung das versenden von Email-
Benachrichtigungen an Kunden, wenn ein Gruppenwechsel vollzogen wird. Für die
Emails können beliebige Templates angelegt und für jede Regel einzeln
konfiguriert werden.


==== INSTALLATION ====
1.) Entpacken Sie das Archiv im Magento Wurzelverzeichnis
2.) Aktualisieren Sie den Konfigurations- und HTML-Block Cache
3.) Loggen Sie sich aus (um die Berechtigungen in der Session zu aktualisieren)
4.) Melden Sie sich erneut im Admin Interface an.

Wenn Sie einen Menü-Eintrag "Group Switcher" unter "Kunden" haben war die
Installation erfolgreich.


==== KONFIGURATION ====
Die Erweiterung ist sehr einfach zu konfigurieren, es gibt nur wenige Optionen.
Sie können die Konfiguration unter
System > Konfiguration > Der Modulprogrammierer > GroupSwitcher
finden.

Die folgenden Optionen stehen zur Verfügung:

- Kunden-Benachrichtigungs Email-Absender
Wählen Sie die Identität zum Verschicken der Benachrichtigung-Emails.
Diese Einstellung hat nur dann einen Effekt wenn Sie beim Erstellen der Regeln
wählen Emails zu verschicken, wenn eine Gruppenzuweisung stattfindet.

- Logging Aktivieren
Wenn aktiviert werden alle Gruppenzuweisungen und evtl. Fehler in der Datei
var/log/groupswitcher.log protokoliert.

- Shop Umsatzsteuer-Identifikationsnummer
Wenn der Shop-Betreiber eine EG Umsatzsteuer-Id besitzt, kann sie hier angegeben
werden um die fortgeschrittenen Umsatzsteuer-Id Validierungs-Typen zu aktivieren
(siehe unten).
Ausserdem wird die online-Validierung durch den Service unter
http://evatr.bff-online.de/ aktiviert, wenn die Umsatzsteuer-Id des Shop-
Betreibers aus Deutschland stammt. Der Dienst unterstützt nur die Validierung
von Umsatzsteuer-Id's aus dem Europäischen Ausland zusammen mit einer deutschen
Umsatzsteuer-Id für den Shop-Betreiber.
Enthält das Feld keine deutsche Umsatzsteuer-Id wird trotzdem immer noch die
syntaktische Validierung für Umsatzsteuer-Id's verwendet wie sie unter
http://ec.europa.eu/taxation_customs/vies/faqvies.do#item11 angegeben ist.


==== ANWENDUNG =====
Damit die Erweiterung überhaupt etwas macht, muss zuerst mindestens eine
GroupSwitcher-Regel erstellt werden. Dies können Sie unter
Kunden > Group Switcher > GroupSwitcher Regeln
erledigen.


=== GroupSwitcher Regeln ===
Hier sehen Sie eine Liste mit allen konfigurierten Regeln. Das erste mal wenn
Sie die Seite besuchen wird die Liste, natürlich, leer sein.
Um eine Regel zu erstellen, klicken Sie den Knopf mir der Aufschrift "Neue Regel
hinzufügen".
Die erste Auswahl die Sie zu treffen haben, ist der Regel-Typ. Um eine Liste mit
einer Beschreibung aller Regel-Typen zu lesen, sehen sie bitte oben im Abschnitt
ÜBER DIE ERWEITERUNG nach.
Nachdem Sie den Regel-Typ ausgewählt haben, werden Sie ein Formular im familiären
Magento Look-and-Feel präsentiert bekommen. Auf der linken Seite sehen Sie drei
Reiter.
Die Optionen unter dem ersten und den dritten Reiter sind für alle Regel-Typen
die gleichen. Der zweite Reiter enthält alle Optionen die spezifisch für den
ausgewählten Regel-Typ sind.
Die meisten Felder sollten selbsterklärend sein, aber falls nicht, folgt nun eine
Liste aller Optionen, sortiert nach den Reitern auf denen sie zu finden sind.


== Optionen auf dem Reiter "Allgemein" ==

- Name
Der Name einer Regel dient nur der einfachen Identifizierung. Am besten ist
es klare, beschreibende Namen zu vergeben.

- Von Gruppe
Ist hier eine Gruppe ausgewählt, wird die Regel nur dann angewendet wen der
Kunde in der ausgewählten Kundengruppe ist.

- Nach Gruppe
Ist hier eine Gruppe ausgewählt, wird der Kunde der ausgewählten Kundengruppe
zugewiesen wenn die Bedingungen der Regel zutrifft.

- Ist Aktiv
Die Regel kann mit dieser Option einfach deaktiviert werden ohne das sie gleich
ganz gelöscht werden muss.

- Priorität
Wenn Sie mehrere mögliche Regeln konfiguriert haben, werden Sie in der Reihenfolge
der Priorität abgearbeitet. Regeln mit einer höheren Priorität werden zuerst
ausgewertet.

- Weitere Regeln ausführen
Wird "Ja" ausgewählt werden keine weiteren Regeln mehr ausgewertet, wenn diese
Regel zutrifft.

- Kunde per Email benachrichtigen
Wenn Sie die Kunden über den Gruppenwechsel per Email benachrichtigen möchten
wählen Sie "Ja".

- Email Template
Diese Option ist nur sichtbar wenn "Kunde per Email benachrichtigen" auf "Ja"
gesetzt ist. Hier kann ein Transaktions-Email Template konfiguriert werden,
welches für die Benachrichtigung verwendet wird. Die GroupSwitcher Erweiterung
beinhaltet ein Standard-Template, das für den Produktiv-Einsatz angepasst
werden muss.

- Kommentar
Dieses Feld ist für Ihre Notizen. Sie sind nur beim Bearbeiten einer Regel
sichtbar. Sie können das Feld auch einfach leer lassen wenn Sie sie nicht
benötigen.


== Optionen auf dem Reiter "Geplante Wechsel" ==

- Geplanten Gruppenwechsel anlegen
Wählen Sie "Ja" um um einen geplanten Gruppenwechsel anzulegen wenn die Regel
angewendet wird. Alle anderen Felder auf diesem Reiter sind nur sichtbar wenn
die Option aktiviert ist.

- Geplanter Gruppenwechsel in N Tagen
Geben Sie die Anzahl Tage an in denen der geplante Gruppenwechsel ausgeführt
werden soll.

- Geplanter Gruppenwechsel: von Gruppe
Der geplante Gruppenwechsel wird nur ausgeführt, wenn der Kunde der spezifizierten
Gruppe angehört. Diese Option dient zur Absicherung gegen ungewollte Zuweisungen.
Wenn der Gruppenwechsel auf jeden Fall ausgeführt werden soll, wählen Sie
"-- Jede Gruppe --" aus.

- Geplanter Gruppenwechsel: nach Gruppe
Der Kunde wird beim Anwenden des geplanten Gruppenwechsels dieser Gruppe
zugewiesen.

- Ersetze bestehende geplante Gruppenwechsel für den Kunden
Falls aktiviert, werden alle anderen geplanten Gruppenzuweisungen für diesen
Kunden durch den neuen Eintrag ersetzt, sobald die Regel angewendet wird.

- Kunde per Email benachrichtigen
Wenn Sie die Kunden über den Gruppenwechsel per Email benachrichtigen möchten
wählen Sie "Ja".

- Email Template
Diese Option ist nur sichtbar wenn "Kunde per Email benachrichtigen" auf "Ja"
gesetzt ist. Hier kann ein Transaktions-Email Template konfiguriert werden,
welches für die Benachrichtigung verwendet wird. Die GroupSwitcher Erweiterung
beinhaltet ein Standard-Template, das für den Produktiv-Einsatz angepasst
werden muss.


== Optionen auf dem Reiter "Regeltyp-Spezifisch" ==

Der Inhalt dieses Reiters hängt von dem Regel-Typ ab.

= Regel-Spezifische Optionen für "Kauf eines Produktes" Regeln =

- Produkt SKU
Geben Sie eine Liste von SKUs an. Kauft ein Kunde eines der Produkte wird er
der konfigurierten Kundengruppe zugewiesen.
Mehrerer SKUs können durch ein Komma gefolgt von einem Leerzeichen getrennt
werden. Ein einzelnes Komma kann Teil einer SKU sein und reicht deswegen nicht
als Trennzeichen aus.

- Status der Bestellung
Die Regel wird nur angewendet wenn die Bestellung in diesem Zustand ist.

- Stores
Die Regel wird nur bei Bestellungen aus den ausgewählten Stores angewendet.
Wenn die Regel global angewendet werden soll, markieren Sie alle Store Views.

= Regel-Spezifische Optionen für "Anzahl Bestellungen" Regeln =

- Die Anzahl der Bestellungen ist
Wählen Sie den Vergleichs-Operator. Zur Auswahl stehen: Gleich, Mehr als,
Weniger als, Mehr als oder gleich, Weniger als oder gleich.
Die Anzahl der Bestellungen des Kunden wird mit diesem Operator mit dem Wert
des "Anzahl der Bestellungen" Feldes verglichen.

- Anzahl der Bestellungen
Die Anzahl der Bestellungen des Kunden wird mit diesem Wert verglichen. Dazu
wird der oben ausgewählte Vergleichs-Operator benutzt.

- Innerhalb der letzten N Tage
Nur Bestellungen innerhalb der angegebenen Anzahl von Tagen werden gewertet.
Lassen Sie das Feld leer oder setzen Sie es einfach auf 0 (Null) wenn sie keine
zeitliche Einschränkung der Regel wünschen.

- Status der Bestellung
Nur Bestellungen in diesem Zustand werden gezählt.

= Regel-Spezifische Optionen für "Zwischensumme einer Bestellung" Regeln =

- Die Zwischensumme der Bestellung ist
Wählen Sie den Vergleichs-Operator. Zur Auswahl stehen: Gleich, Mehr als,
Weniger als, Mehr als oder gleich, Weniger als oder gleich.
Die Zwischensumme der Bestellung wird mit diesem Operator mit dem Wert
des "Menge" Feldes verglichen.

- Menge
Die Zwischensumme der Bestellung wird mit diesem Wert verglichen. Dazu wird der
oben ausgewählte Vergleichs-Operator benutzt.

- Status der Bestellung
Die Regel wird nur für Bestellungen in diesem Zustand angewendet.

- Stores
Die Regel wird nur bei Bestellungen aus den ausgewählten Stores angewendet.
Wenn die Regel global angewendet werden soll, markieren Sie alle Store Views.


= Regel-Spezifische Optionen für "Umsatz eines Kunden" Regeln =

- Der Gesamtumsatz für den Kunden ist
Wählen Sie den Vergleichs-Operator. Zur Auswahl stehen: Gleich, Mehr als,
Weniger als, Mehr als oder gleich, Weniger als oder gleich.
Der Zwischensummen-Gesamtumsatz des Kunden wird mit diesem Operator mit dem Wert
des "Menge" Feldes verglichen.

- Innerhalb der letzten N Tage
Nur Bestellungen innerhalb der angegebenen Anzahl von Tagen werden gewertet.
Lassen Sie das Feld leer oder setzen Sie es einfach auf 0 (Null) wenn sie keine
zeitliche Einschränkung der Regel wünschen.

- Status der Bestellung
Nur Bestellungen in diesem Zustand werden beim Berechnen des Umsatzes gewertet.

- Stores
Nur Bestellungen aus den ausgewählten Stores werden beim Berechnen des
Umsatzes verwendet. Wenn die Regel global angewendet werden soll, markieren Sie
alle Store Views.

= Regel-Spezifische Optionen für "Kunden-Attribut" Regeln =

- Kunden-Attribut
Wählen Sie das Kunden Attribut für die Regel.

- Wert
Geben Sie den Vergleichs-Wert ein. Je nachdem was unter "Wert Ausdruck Art"
ausgewählt wird, wird die Eingabe anders ausgewertet.

- Wert Ausdruck Art
Wählen Sie die Art des Vergleichs-Wert. Verfügbar sind: Einfacher Wert, Regulärer
Ausdruck und Model Callback

Wenn Sie "Einfacher Wert" auswählen, wird die Regel angewendet wenn der Wert des
Kunden-Attributes mit dem von Ihnen angegebenen Wert identisch ist.

Wenn Sie "Regulärer Ausdruck" auswählen, wird das Kunden-Attribut mit dem unter
"Wert" angegeben Text als Regex geprüft. Reguläre Ausdrücke sind ein komplexes
Thema das nicht in diesem Dokument behandelt wird. Eine weiterführende Anleitung
kann unter http://www.php.net/manual/de/reference.pcre.pattern.syntax.php
gefunden werden.
Eine Regulärer Ausdruck könnte zum Beispiel folgendes sein: /[a-Z0-5]{2,6}/
Dieser Ausdruck würde auf 2-6 Buchstaben und/oder Zahlen von 0 bis 5 passen.

Falls Sie Erfahrung mit dem Entwickeln von Magento-Erweiterungen haben, können
Sie auch "Model Callback" als Art des Ausdrucks wählen. Sie Syntax dafür ist
module/model::method
Entwickler-Information: Es werden drei Parameter an die Callback-Methode
übergeben. Der erste ist das Kunden-Model, der zweite ist der Attribut-Code
des zu vergleichenden Kunden-Attributes, und der dritte ist das Rule Model.
Diese Option bietet die größte Flexibilität, aber sie müssen einiges an Vorwissen
mitbringen. Das Erstellen von Magento-Modulen wird nicht in diesem Dokument
behandelt, aber es gibt eine Vielzahl an Online-Anleitungen, wie zum Beispiel
http://www.magentocommerce.com/knowledge-base/entry/magento-for-dev-part-1-introduction-to-magento

= Regel-Spezifische Optionen für "Umsatzsteuer-Identifikationsnummer" Regeln =

- Umsatzsteuer-ID Attribut
Wählen Sie das Kunden-Attribut welches die Umsatzsteuer-Identifikationsnummer
speichert. Der Magento-Standard ist "taxvat" (TAX/VAT Number).

- Regel anwenden wenn
Wählen Sie unter welchen Umständen die Regel angewendet werden soll.

Wählen Sie "Gültige EG-Ausland Umsatzsteuer-ID" damit die Regel zutrifft, wenn
ein Kunde eine gültige Umsatzsteuer-ID aus einem anderen Land als die des
Shop-Betreibers eingibt. Die Umsatzsteuer-ID des Shop-Betreibers wird unter
"System > Konfiguration > GroupSwitcher > Shop Umsatzsteuer-Identifikationsnummer"
angegeben. Ist dort keine Umsatzsteuer-ID hinterlegt steht diese Variante im
Dropdown nicht zur Verfügung.
Bei der Variante "Keine gültige EG-Ausland Umsatzsteuer-ID" wird die Regel
angewendet wenn entweder die Umsatzsteuer-ID des Kunden ungültig ist, oder wenn
sie aus dem gleichen Land wie die des Shop-Betreibers stammt.
Um die Regel bei einer gültigen Umsatzsteuer-ID aus dem Inland zu aktivieren
wählen Sie "Gültige inländische Umsatzsteuer-ID".
Um die Regel anzuwenden bei einer ungültigen Umsatzsteuer-ID oder einer ID aus
dem Ausland wählen Sie "Keine gültige inländische Umsatzsteuer-ID".
Die vorangegangenen vier Varianten stehen nur zur Verfügung wenn die
Shop-Betreiber Umsatzsteuer-ID angegeben wurde. Die beiden folgenden Varianten
stehen immer zur Verfügung.
"Umsatzsteuer-ID gültig" trifft auf alle Kunden mit einer gültigen Umsatzsteuer-
ID zu, egal aus welchem Land die ID stammt.
"Umsatzsteuer-ID ungültig" trifft auf alle Kunden mit einer ungültigen
Umsatzsteuer-ID zu.

=== Geplante Gruppenwechsel ===

Die geplanten Gruppenwechsel können verwaltet werden, wenn Sie sich im Admin
Interface anmelden und dann im Menü "Kunden > Group Switcher > Geplante
Gruppenwechsel" auswählen.

Auf der ersten Seite sehen Sie eine Liste aller geplanten Gruppenwechsel.
Die meisten der geplanten Wechsel werden wahrscheinlich bei GroupSwitcher Regeln
erstellt werden. Wenn nötig kann das aber auch manuell erfolgen.

Damit die geplanten Gruppenwechsel ausgeführt werden, müssen Cron-Jobs für
Magento eingerichtet sein. Das Einrichten von Magento Cron-Jobs wird nicht in
diesem Dokument behandelt, eine weiterführende Dokumentation kann aber im Magento
Wiki gefunden werden:
http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/how_to_setup_a_cron_job

Wenn Sie einen neuen geplanten Gruppenwechsel anlegen, oder einen bestehenden
bearbeiten, stehen ihnen die folgenden Felder zur Verfügung:

- Kunde
Der Kunde kann spezifiziert werden indem entweder eine Kunden-ID oder die Email-
Adresse angegeben wird.

- Website ID
Damit ein Kunde eindeutig über seine Email Adresse bestimmt werden kann, muss
zusätzlich die Website welcher der Kunde zugeordnet ist, angegeben werden.

- Gruppenwechsel am
Das Datum an dem der geplante Wechsel ausgeführt wird. Eine Datums-Auswahl steht
zur Verfügung wenn auf das Viereck neben dem Feld geklickt wird.

- Von Gruppe
Der geplante Gruppenwechsel wird nur ausgeführt wenn der Kunde zum geplanten
Zeitpunkt ein Mitglied der ausgewählten Kundengruppe zugewiesen ist.
Wenn sie den Wechsel immer durchführen wollen, egal welcher Kundengruppe der
Kunde gerade zugeordnet ist, wählen Sie "-- Jede Gruppe --".

- Nach Gruppe
Der Kunde wird der ausgewählten Kundengruppe zugeordnet wenn der geplante
Wechsel ausgeführt wird.

- Kunde per Email benachrichtigen
Wenn Sie die Kunden über den Gruppenwechsel per Email benachrichtigen möchten
wählen Sie "Ja".

- Email Template
Diese Option ist nur sichtbar wenn "Kunde per Email benachrichtigen" auf "Ja"
gesetzt ist. Hier kann ein Transaktions-Email Template konfiguriert werden,
welches für die Benachrichtigung verwendet wird. Die GroupSwitcher Erweiterung
beinhaltet ein Standard-Template, das für den Produktiv-Einsatz angepasst
werden muss.

- Kommentar
Wenn Sie wollen können Sie hier ein Kommentar notieren. Geplante Wechsel, welche
von GroupSwitcher Regeln erstellt werden, wird automatisch ein Kommentar
hinzugefügt, in dem der Name und die ID der Regel stehen. Sobald ein geplanter
Gruppenwechsel ausgeführt wird, fügt die Erweiterung diese Information ebenfalls
automatisch zu dem Kommentar hinzu.


==== SUPPORT ====
Deutsch: http://www.der-modulprogrammierer.de/hilfeseiten/hilfe/groupswitcher-de.html
English: http://www.extensionprogrammer.com/hilfeseiten/hilfe/groupswitcher-en.html


==== BUGS ====
Wenn Sie Ideen zur Verbesserung der Erweiterung haben oder Bugs finden, schicken
Sie diese bitte per Email an info@der-modulprogrammierer.de, mit dem Text
DerModPro_GroupSwitcher als Teil des Betreffs.
